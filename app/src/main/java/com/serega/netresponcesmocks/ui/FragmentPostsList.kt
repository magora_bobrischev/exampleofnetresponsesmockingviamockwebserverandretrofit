package com.serega.netresponcesmocks.ui

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.serega.netresponcesmocks.model.EntityPost
import com.serega.netresponcesmocks.R
import com.serega.netresponcesmocks.vm.VmPostsList
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_posts_list.*
import org.koin.android.viewmodel.ext.viewModel

class FragmentPostsList : Fragment() {
    private val viewModel: VmPostsList by viewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_posts_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.loadPosts()

        viewModel.liveData.observe(viewLifecycleOwner, Observer { posts ->
            if (posts != null) {
                onDataLoaded(posts)
            }
        })

        postsList.layoutManager = LinearLayoutManager(context)
    }

    private fun onDataLoaded(data: List<EntityPost>) {
        postsList.adapter = Adapter(data)
    }

    private class Holder(view: View) : RecyclerView.ViewHolder(view) {
        private val tvName = view.findViewById<TextView>(R.id.tvNickname)
        private val image = view.findViewById<ImageView>(R.id.postPhoto)
        private val tvDesc = view.findViewById<TextView>(R.id.tvDescription)

        fun bind(data: EntityPost) {
            tvName.text = data.authorNickname
            tvDesc.text = data.description
            Picasso.get().load(data.imageUrl).into(image)
        }
    }

    private class Adapter(private val data: List<EntityPost>) : RecyclerView.Adapter<Holder>() {
        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): Holder =
            Holder(LayoutInflater.from(p0.context).inflate(R.layout.holder_post, p0, false))

        override fun onBindViewHolder(holder: Holder, position: Int) {
            holder.bind(data[position])
        }

        override fun getItemCount(): Int = data.size
    }
}