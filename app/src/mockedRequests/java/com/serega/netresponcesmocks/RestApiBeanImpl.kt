package com.serega.netresponcesmocks

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.jakewharton.retrofit2.converter.kotlinx.serialization.serializationConverterFactory
import com.serega.netresponcesmocks.model.EntityPost
import com.serega.netresponcesmocks.model.EntityProfile
import com.serega.netresponcesmocks.network.RestApi
import com.serega.netresponcesmocks.network.RestApiBean
import com.serega.netresponcesmocks.network.await
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import retrofit2.Retrofit
import java.net.HttpURLConnection
import java.net.InetAddress

private const val PORT = 8888

class RestApiBeanImpl : RestApiBean {
    private val restApi: RestApi

    init {
        val okHttp = OkHttpClient.Builder().build()

        val retrofit = Retrofit.Builder()
            .client(okHttp)
            .baseUrl(BuildConfig.REST_API_ENDPOINT)
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addConverterFactory(serializationConverterFactory(MediaType.parse("application/json")!!, Json))
            .build()

        restApi = retrofit.create(RestApi::class.java)
    }

    override suspend fun login(login: String, password: String): EntityProfile = coroutineScope {
        mocked(rawEntityMyProfile.mocked()) {
            restApi.login(login, password).await()
        }
    }

    override suspend fun getPosts(): List<EntityPost> = coroutineScope {
        mocked(rawPostsList.mocked()) {
            restApi.getPosts().await()
        }
    }

    private fun String.mocked(code: Int = HttpURLConnection.HTTP_OK) = MockResponse().apply {
        setResponseCode(code)
        setBody(this@mocked)
    }

    private suspend inline fun <T> mocked(response: MockResponse, crossinline block: suspend () -> T): T =
        MockWebServer().use { ws ->
            withContext(Dispatchers.IO) { ws.start(InetAddress.getLocalHost(), PORT) }
            ws.enqueue(response)
            val result = block()
            delay(800)
            ws.takeRequest()
            return result
        }
}