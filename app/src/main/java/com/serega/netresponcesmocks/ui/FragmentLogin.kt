package com.serega.netresponcesmocks.ui

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.serega.netresponcesmocks.R
import com.serega.netresponcesmocks.vm.VmLogin
import kotlinx.android.synthetic.main.fragment_login.*
import org.koin.android.viewmodel.ext.viewModel

class FragmentLogin : Fragment() {
    private val vmLogin: VmLogin by viewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_login, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnLogin.setOnClickListener { validateAndLogin() }

        vmLogin.liveData.observe(viewLifecycleOwner, Observer { profile ->
            if (profile != null) {
                Toast.makeText(context!!, "Profile: $profile", Toast.LENGTH_SHORT).show()
                (activity as? MainActivity)?.onLogined(profile)
                vmLogin.liveData.value = null
            }
        })
    }

    private fun validateAndLogin() {
        val login = etLogin.text.toString()
        val password = etPassword.text.toString()
        if (login.isEmpty().not() && password.isEmpty().not()) {
            vmLogin.login(login, password)
        }
    }
}