package com.serega.netresponcesmocks.network

import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.suspendCancellableCoroutine
import retrofit2.Call
import retrofit2.Callback
import retrofit2.HttpException
import retrofit2.Response
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

fun CoroutineScope.launchCatching(
    dispatcher: CoroutineContext = this.coroutineContext,
    doOnError: (Throwable) -> Unit,
    block: suspend () -> Unit
) = launch(dispatcher + CoroutineExceptionHandler { _, t -> doOnError(t) }) {
    block()
}

suspend fun <T : Any> Call<T>.await(): T =
    suspendCancellableCoroutine { continuation ->
        enqueue(object : Callback<T> {
            override fun onResponse(call: Call<T>?, response: Response<T?>) {
                if (response.isSuccessful) {
                    continuation.resume(response.body()!!)
                } else {
                    continuation.resumeWithException(HttpException(response))
                }
            }

            override fun onFailure(call: Call<T>, t: Throwable) {
                // Don't bother with resuming the continuation if it is already cancelled.
                if (continuation.isCancelled.not()) {
                    continuation.resumeWithException(t)
                }
            }
        })

        continuation.invokeOnCancellation {
            kotlin.runCatching { cancel() }
        }
    }
