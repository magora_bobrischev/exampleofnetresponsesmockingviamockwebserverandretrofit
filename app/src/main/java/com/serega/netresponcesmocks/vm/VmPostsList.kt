package com.serega.netresponcesmocks.vm

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.widget.Toast
import com.serega.netresponcesmocks.model.EntityPost
import com.serega.netresponcesmocks.network.RestApiBean
import com.serega.netresponcesmocks.network.launchCatching
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlin.coroutines.CoroutineContext

class VmPostsList(app: Application, private val restApi: RestApiBean) : AndroidViewModel(app), CoroutineScope {
    private val sJob = SupervisorJob()
    override val coroutineContext: CoroutineContext get() = Dispatchers.Main + sJob
    val liveData = MutableLiveData<List<EntityPost>>()

    fun loadPosts() {
        launchCatching(doOnError = ::onError) {
            val posts = restApi.getPosts()
            liveData.value = posts
        }
    }

    private fun onError(t: Throwable) {
        Toast.makeText(getApplication(), t.message, Toast.LENGTH_SHORT).show()
    }

    override fun onCleared() {
        sJob.cancel()
        super.onCleared()
    }
}