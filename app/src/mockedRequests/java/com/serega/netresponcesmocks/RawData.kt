package com.serega.netresponcesmocks

import java.util.*

val rawEntityMyProfile = """
    {
        "id":"${UUID.randomUUID()}",
        "nickname":"MegaDzhigurda",
        "avatarUrl":null
    }
""".trimIndent()

val rawPostsList = """
    [
        {
            "authorNickname":"NickName01",
            "description":"Nunc euismod vehicula purus vel eleifend. Sed consectetur porta augue, nec tincidunt ipsum feugiat laoreet. Nulla porta turpis in viverra ornare.",
            "id":"9393f17c-ea73-491e-bff1-fc5ffec79ed7",
            "imageUrl":"https://assets3.thrillist.com/v1/image/2528243/size/tmg-article_tall;jpeg_quality\u003d20.jpg"
        },
        {
            "authorNickname":"NickName02",
            "description":"Nunc venenatis ligula mi, suscip",
            "id":"8cb0e84a-9e07-45f2-8df9-c53ada7430df",
            "imageUrl":"https://encrypted-tbn0.gstatic.com/images?q\u003dtbn:ANd9GcSe8Jz9HWznGSe5UmiA7aTpyr4h0ZbU6kMaNVtLoe1bzXJePHXG"
        },
        {
            "authorNickname":"NickName03",
            "description":"enean semper risus ut purus sollicitudin, ac ultrices libero mollis. Ut in neque at eros sollicitudin finibus",
            "id":"faceb170-efae-4791-81ab-b1049adcfc80",
            "imageUrl":"https://s-i.huffpost.com/gen/1549129/images/o-KITTYFINGER-facebook.jpg"
        },
        {
            "authorNickname":"NickName04",
            "description":"Morbi eget tortor vitae est gravida efficitur. Donec posuere nulla vel nisl tempor finibus.",
            "id":"b818e4dc-1653-4c4d-bc6a-c7856077dff4",
            "imageUrl":"https://cdn.playbuzz.com/cdn/f5b92d65-9689-44c9-97b5-bcaa66b2bc26/83ec1d2e-7c28-4fd7-91f0-5c138c064225.jpg"
        },
        {
            "authorNickname":"NickName05",
            "description":"vitae ex congue pharetra. Sed cursus ipsum ut vestibulum efficitur.",
            "id":"77047918-2bcf-4948-a724-98b1d2dd8a8b",
            "imageUrl":"https://encrypted-tbn0.gstatic.com/images?q\u003dtbn:ANd9GcTPpSlM6R4lf4KxQAXn6beUTBuOHym8GCkE3A-VkkYX5vIuQH64"
        },
        {
            "authorNickname":"NickName06",
            "description":"Etiam suscipit, eros ut gravida molestie, dui dui hendrerit ante, ac condimentum leo enim et erat. Praesent ut tellus id lacus porta posuere non vitae tortor. Donec a mattis odio, ut euismod metus. Praesent tincidunt orci tellus, vel mollis quam imperdiet eu. Curabitur non ipsum sit amet justo aliquet vehicula",
            "id":"0ed02c3d-bd25-44ef-bd8b-4090012b5c6a",
            "imageUrl":"https://i.kym-cdn.com/photos/images/newsfeed/001/384/531/8ed.jpg"
        },
        {
            "authorNickname":"NickName07",
            "description":"Nunc venenatis ligula mi, suscipit vehicula mauris tempor eu. Morbi sem est, varius id egestas eleifend, lacinia ac tortor. Vivamus vel augue luctus, vestibulum ipsum quis, sodales elit. Donec at felis ut sapien sagittis auctor. Vestibulum convallis, sem laoreet mollis tempor, ipsum metus tincidunt mauri",
            "id":"5d2fa628-1856-4884-9d10-6d3856565c1f",
            "imageUrl":"https://encrypted-tbn0.gstatic.com/images?q\u003dtbn:ANd9GcQG_JXXj-O2y_dKKiVsduAjKkBDmD3_wPQtehjLgUzuyBbGUVLXeg"
        },
        {
            "authorNickname":"NickName08",
            "description":"mollis et. Aliquam rutrum ante vitae ex congue",
            "id":"da88a457-a711-4a25-9d73-4e8b9a7f97ab",
            "imageUrl":"https://encrypted-tbn0.gstatic.com/images?q\u003dtbn:ANd9GcQflUXeSRnCFWiMNrtKLTVfA5IXmP1EcwPpL8wdaJsDD-u_mtJQ"
        },
        {
            "authorNickname":"NickName09",
            "description":" Cras consequat ultricies nunc non lacinia. Maecenas id tortor vitae felis fringilla scelerisque in ac risus. Maecenas eleifend nec eros quis",
            "id":"0831ca48-4c6b-4589-a6c8-42c5686cbf50",
            "imageUrl":"https://encrypted-tbn0.gstatic.com/images?q\u003dtbn:ANd9GcQk5q0_1k4ZP-I2RxAvd_nbGQ4rOXh0prxFATXTIQnjfLprfmVmlw"
        },
        {
            "authorNickname":"NickName10",
            "description":"",
            "id":"c2e165bd-6bd1-43a4-a2b9-b29c378f743c",
            "imageUrl":"https://encrypted-tbn0.gstatic.com/images?q\u003dtbn:ANd9GcTChUZVOHe0qWl7ivLXU8phBnxGdU2w2zjLadU7u2vDBZ4U5M_5MQ"
        }
    ]""".trimIndent()

