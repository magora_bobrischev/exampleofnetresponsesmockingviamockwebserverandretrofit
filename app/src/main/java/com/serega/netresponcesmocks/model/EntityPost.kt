package com.serega.netresponcesmocks.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class EntityPost(
    @SerialName("id") val id: String,
    @SerialName("imageUrl") val imageUrl: String,
    @SerialName("authorNickname") val authorNickname: String,
    @SerialName("description") val description: String
)
