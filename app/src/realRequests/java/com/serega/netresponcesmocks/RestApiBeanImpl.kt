package com.serega.netresponcesmocks

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.jakewharton.retrofit2.converter.kotlinx.serialization.serializationConverterFactory
import kotlinx.coroutines.coroutineScope
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import okhttp3.OkHttpClient
import retrofit2.Retrofit

class RestApiBeanImpl : RestApiBean {
    private val restApi: RestApi

    init {
        val okHttp = OkHttpClient.Builder().build()

        val retrofit = Retrofit.Builder()
            .client(okHttp)
            .baseUrl(BuildConfig.REST_API_ENDPOINT)
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addConverterFactory(serializationConverterFactory(MediaType.parse("application/json")!!, Json))
            .build()

        restApi = retrofit.create(RestApi::class.java)
    }

    override suspend fun login(login: String, password: String): EntityProfile = coroutineScope { restApi.login(login, password).await() }

    override suspend fun getPosts(): List<EntityPost> = coroutineScope { restApi.getPosts().await() }
}