package com.serega.netresponcesmocks.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.serega.netresponcesmocks.model.EntityProfile

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportFragmentManager.beginTransaction()
            .replace(android.R.id.content, FragmentLogin())
            .commitNow()
    }

    fun onLogined(profile: EntityProfile) {
        supportFragmentManager.beginTransaction()
            .replace(android.R.id.content, FragmentPostsList())
            .addToBackStack(null)
            .commit()
    }
}
