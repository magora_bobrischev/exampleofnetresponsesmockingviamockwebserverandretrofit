package com.serega.netresponcesmocks.network

import com.serega.netresponcesmocks.model.EntityPost
import com.serega.netresponcesmocks.model.EntityProfile

interface RestApiBean {

    suspend fun login(login: String, password: String): EntityProfile

    suspend fun getPosts(): List<EntityPost>
}