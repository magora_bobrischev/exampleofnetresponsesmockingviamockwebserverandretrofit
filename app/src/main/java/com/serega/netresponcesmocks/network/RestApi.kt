package com.serega.netresponcesmocks.network

import com.serega.netresponcesmocks.model.EntityPost
import com.serega.netresponcesmocks.model.EntityProfile
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface RestApi {

    @POST("auth")
    fun login(@Query("loginAsync") login: String, @Query("password") password: String): Call<EntityProfile>

    @GET("posts")
    fun getPosts(): Call<List<EntityPost>>

}