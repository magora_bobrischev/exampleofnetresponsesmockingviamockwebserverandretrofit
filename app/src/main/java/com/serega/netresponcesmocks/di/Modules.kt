package com.serega.netresponcesmocks.di

import com.serega.netresponcesmocks.RestApiBeanImpl
import com.serega.netresponcesmocks.network.RestApiBean
import com.serega.netresponcesmocks.vm.VmLogin
import com.serega.netresponcesmocks.vm.VmPostsList
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.bind
import org.koin.dsl.module

val restModule = module {
    single { RestApiBeanImpl() } bind RestApiBean::class
}

val vmModule = module {
    viewModel { VmLogin(app = get(), restApi = get()) }
    viewModel { VmPostsList(app = get(), restApi = get()) }
}
