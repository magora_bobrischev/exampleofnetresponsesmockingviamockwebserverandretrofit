package com.serega.netresponcesmocks

import android.app.Application
import com.serega.netresponcesmocks.di.restModule
import com.serega.netresponcesmocks.di.vmModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@App)
            modules(restModule, vmModule)
        }
    }
}