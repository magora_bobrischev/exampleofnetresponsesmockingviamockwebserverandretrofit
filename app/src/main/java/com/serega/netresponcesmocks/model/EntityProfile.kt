package com.serega.netresponcesmocks.model

import kotlinx.serialization.Optional
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class EntityProfile(
    @SerialName("id") val id: String,
    @SerialName("nickname") val nickname: String,
    @SerialName("avatarUrl") @Optional val avatarUrl: String? = null
)